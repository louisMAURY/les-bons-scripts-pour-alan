# Le petit script pour Alan

---
---

## Le juste prix (nommé "justprice")

---

### Qu'est-ce que c'est ?
`justprice` est un script qui n'est de plus que le jeu du **juste prix** (qui était présenté par Vincent Lagaf'). Ce jeu consiste à trouvé "le juste prix" qui (dans notre cas) est un nombre aléatoire en 1 et 1000.
Si le nombre que l'on donne est plus grand que le juste prix, le programme nous que "c'est moins" et "C'est plus" si...
> Il est plus petit ?

C'est bien !
Et quand le juste prix est trouvé, c'est gagné.

### Où est-ce que je l'ai rangé ?
Pour qu'il puisse être utilisé partout, je l'ai situé dans l'arborescence `/bin`

### Comment l'utiliser ?
Il suffit juste de taper la commande `justprice`:
```
user@computer:~$ justprice
Entrez un prix:
4
C'est plus
Entrez un prix:
^C
```
### Comment je l'ai testé ?
Je me suis mis dans la peau d'un utilisateur :
*Pour chacunes de ces entrées le script exprime son mécontentement.*
```
Entrez un prix:
b4
Entrez un prix:
4b
Entrez un prix:
a
Entrez un prix:

Entrez un prix:
ab
Entrez un prix:
4.5
Entrez un prix:
.
```

---
---

## Le Youtube dl
### Qu'est-ce que c'est ?
C'est un script qui telecharge l'audio d'une vidéo youtube et l'enregistre dans le dossier `Music`.

### Où est-ce que je l'ai rangé ?
Au même endroit que `justprice` et pour les même raisons.

### Comment l'utiliser ?
Il faut taper la commande `youtube_download`.